'use strict';

var templateThis = this;

(function () {

  var hoganTemplates = templateThis["templates"];

  // vars vars vars
  var mapboxUrl = 'https://a.tiles.mapbox.com/v3/mi.0ad4304c/{z}/{x}/{y}.png';
  var attribution = '<a href="https://www.mapbox.com/about/maps/" target="_blank">© Mapbox © OpenStreetMap</a> | Arturo Cullinane | Blanc Ltd ©'

  // where the hell is that data coming from?
  var DATA_URL = "/data/gameofthrones.json";

  // get default lat and lng
  var lat = 36.672825;
  var lng = -76.541748;

  // define CORS header if we decide to use different server for data
  var initHeaders = new Headers();
  var requestInit = { method: 'GET',
               headers: initHeaders,
               mode: 'cors',
               cache: 'default' };

  // set bounds to the wooooorld!
  var southWest = L.latLng(85, -180),
      northEast = L.latLng(-85, 180),
      bounds = L.latLngBounds(southWest, northEast);

  // initialize map
  var map = L.map('map', {
    zoomControl : true,
    maxBounds: bounds,
    maxZoom: 19,
    minZoom: 2
  })// .setView([lat, lng], 12);
  map.fitBounds(bounds);
  L.tileLayer(mapboxUrl, { maxZoom: 19, attribution: attribution}).addTo(map);
  L.control.scale({ metric : true, imperial : false }).addTo(map);

  // on open center popup
  map.on('popupopen', function(e) {
    var px = map.project(e.popup._latlng);
    px.y -= e.popup._container.clientHeight/2;
    map.panTo(map.unproject(px),{ animate: true });
  });

  // Add those daaaaamn layers
  var markers                 = L.featureGroup().addTo(map);
  var sourcePoints            = L.featureGroup().addTo(map);
  var reachablePoints         = L.featureGroup().addTo(map);
  var routeLayer              = L.featureGroup().addTo(map);
  var sourceAndReachableLayer = L.featureGroup();

  // get JSON
  $.getJSON(DATA_URL, function( data ) {
    // loop through that data
    data.forEach(function (value) {
      var id = value.id;
      var lat = value.lat;
      var lng = value.lng;
      var isStory = (typeof value.isStory === "boolean") ? value.isStory : false;
      var imgLink = (typeof value.img === "string") ? value.img : '/images/popupImages/placeholder.jpg';
      var title = (typeof value.title === "string") ? value.title : 'Placeholder Title';
      var description = (typeof value.description === "string") ? value.description : "Placeholder Description";
      var exists = (value.title !== undefined && value.description !== undefined && typeof value.img !== undefined) ? true : false;
      addMarker(exists, id, lat, lng, isStory, imgLink, title, description, markers, false, false, map, DATA_URL);
    });
  })
  .error(function(err) { console.log("error getting the JSON data yo " + err); });


  // event handlers
  $(function () {

    // open read-more
    $('#popup-info').on('click', '.readMore', function (event) {
      event.preventDefault();
      if ($('#map-info').hasClass('show')) {
        $('#map-info').removeClass('show');
        $('.map-info-title').removeClass('animated fadeInUp');
        $('.map-info-img').removeClass('animated fadeInUp');
        $('.map-info-description').removeClass('animated fadeInUp');

      } else {
        $('#map-info').addClass('show');
        $('.map-info-title').addClass('animated fadeInUp');
        $('.map-info-img').addClass('animated fadeInUp');
        $('.map-info-description').addClass('animated fadeInUp');
      }

      var title = $(this).data('title');
      var description = $(this).data('desc');
      var imgLink = $(this).data('img');
      var id = $(this).data('id');
      editTemplate(DATA_URL, id);
    });

    // next prev
    $('#map-info').on('click', '.skip', function (event) {
        event.preventDefault();
        var id = parseInt($(this).attr('data-id'));
        editTemplate(DATA_URL, id);

        if ($(this).hasClass('prev')) {
            $('.map-info-slider').addClass('animated slideOutLeft');
            setTimeout(function () {
              $('.map-info-slider').removeClass('slideOutLeft').addClass('slideInRight');
            }, 10);
        }

        if ($(this).hasClass('next')) {
            $('.map-info-slider').addClass('animated slideOutRight');
            setTimeout(function () {
              $('.map-info-slider').removeClass('slideOutRight').addClass('slideInLeft');
            }, 10);
        }
    });

    // close read-more
    $("#map-info").on('click', '.close', function (event) {
      event.preventDefault();
      $('#map-info').removeClass('show');
      $('.map-info-title').removeClass('animated fadeInUp');
      $('.map-info-img img').removeClass('animated fadeInUp');
      $('.map-info-description').removeClass('animated fadeInUp');
    });

    // close popup
    $('#popup-info').on('click', '.close', function (event) {
      event.preventDefault();
      $('#popup-info').removeClass('animated slideInRight').addClass('animated slideOutRight');
    })
  });








  /**
   *
   *  All those functions
   */

  // add Marker
  function addMarker(exists, id, lat, lng, isStory, imgLink, title, description, sourcePoints, clear, source, map, DATA_URL) {
    var popup = setPopup(map, lat, lng);
    var customOptions = {
      'maxWidth': '500',
      'className' : 'custom'
    };

    if (clear) {
      sourcePoints.clearLayers();
    }

    if (source) {
      return L.marker(
          [lat, lng],
          { draggable : true, icon : L.icon({
            iconUrl: '/images/marker/home.png',
            shadowUrl: '/images/marker/markers-shadow.png',
            iconSize: [35, 45],
            iconAnchor:   [17, 42],
            popupAnchor: [1, -32],
            shadowAnchor: [10, 12],
            shadowSize: [36, 16],
          })}
      )
      .addTo(sourcePoints);
    }

    if (!source) {
      var isStory     = (typeof isStory == "boolean") ? isStory : false;
      var icon        = isStory ? 'heart' : 'star';
      var markerColor = isStory ? 'green' : 'black';
      return L.marker(
          [lat, lng],
          {
            draggable : false,
            icon : L.AwesomeMarkers.icon({ icon: icon, prefix : 'fa', markerColor: markerColor })
          }
      )
      .bindPopup(popup, customOptions)
      .on('click', function (event) {
        if(exists) {
          event.target.closePopup();
          editPopup(DATA_URL, id);
        } else {
          event.target.closePopup();
        }
      })
      .on('mouseover', function (event) { })
      .addTo(sourcePoints);
    }

    if(typeof lat !== "number" && typeof lng !== "number") {
      console.log('lat or lng is not a number')
      console.log('typeof lat', typeof lat);
      console.log('typeof lng', typeof lng);
    }
  }

  // setPopup
  function setPopup(map, lat, lng) {
    var latlng = new L.LatLng(lat, lng);
    var popup = L.popup({
      keepInView: false,
      maxWidth: 300,
      minWidth: 300,
      autoPanPadding: 0.5
    })
    .setLatLng(latlng);
    return popup;
  }

  // custom popup
  function editPopup(data, id) {
    $('#popup-info').removeClass('slideOutRight').addClass('show animated slideInRight');

    var id = parseInt(id, 10);

    $.getJSON(data, function (result) {
      result.map(function (val) {
        var dataid = parseInt(val.id, 10);
        if(dataid === id) {
          var desc = val.short_desc.substring(0, 140) + ' . . .';
          val.short_desc = desc;
          var html = hoganTemplates.popupTpl.render(val);
          console.log('html', html);
          $('#popup-info').html(html);
        }
      });
    });
  }

  // function update info template
  function editTemplate(data, id) {
    var id = parseInt(id, 10);
    var filters = [];

    $.getJSON(data, function(result) {
      filters = result.filter(function (value) {
        return (value.title !== undefined && value.description !== undefined && value.location !== undefined && value.img !== undefined);
      });
      var len = filters.length - 1;
      filters.map(function (val) {
        var dataid = parseInt(val.id, 10);
        if(dataid === id) {
          var nextId = (dataid < len) ? dataid + 1 : 0;
          var prevId = (dataid > 0) ? dataid - 1 : len;
          val.nextId = nextId;
          val.prevId = prevId;
          var html = hoganTemplates.mapTpl.render(val);
          $('#map-info-slider').html(html);
        }
      });
    })
    .error(function(err) { alert("error getting the json data on cycle yo " + err); })
  }

})();
