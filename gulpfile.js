var gulp = require('gulp');
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var browserSync = require('browser-sync').create();
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var htmlmin = require('gulp-htmlmin');
var hoganCompiler = require('gulp-hogan-precompile');
var declare = require('gulp-declare');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var jsonminify = require('gulp-jsonminify');

// hogan compilers
gulp.task('templates', function () {
  return gulp.src('./app/templates/**/*.html')
    .pipe(hoganCompiler())
    .pipe(declare({
      namespace: 'templates',
      noRedeclare: true
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('./app/js/'));
});

// browser reload
gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: 'app'
    }
  });
});

// sass preprocessor
gulp.task('sass', function () {
  return gulp.src('app/scss/styles.scss')
    .pipe(sass())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// minify css
gulp.task('cssminifier', function() {
  return gulp.src('app/css/**/*.css')
      .pipe(cssnano())
      .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
      .pipe(gulp.dest('./dist/css'))
});

// minify js
gulp.task('jsconcat', function () {
  return gulp.src([
      './app/js/lib/jquery.2.min.js',
      './app/js/lib/mustache.js',
      './app/js/lib/hogan.js',
      './app/js/lib/leaflet.js',
      './app/js/lib/leaflet.awesome-markers.min.js',
      './app/js/templates.js',
      './app/js/super-core.js'
    ])
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest('./app/js'))
});

// minify js
gulp.task('jsminifier', function () {
  return gulp.src('./app/js/main.min.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js'))
});

// minify and build data
gulp.task('dataminifier', function () {
  return gulp.src('./app/data/**/*.json')
    .pipe(jsonminify())
    .pipe(gulp.dest('./dist/data'))
});

// js and css minification
gulp.task('useref', function () {
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

// html
gulp.task('optimize:html', function () {
  return gulp.src('app/*.html')
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(gulp.dest('dist/'))
});

// fonts
gulp.task('fonts', function () {
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
});

// images minification
gulp.task('images', function () {
  return gulp.src('app/images/**/*.+(jpg|jpeg|png|svg|gif)')
    .pipe(cache(imagemin({
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
});

// watch
gulp.task('watch', function () {
  gulp.watch('./app/scss/**/*.scss', ['sass']);
  gulp.watch('./app/js/**/*.js', browserSync.reload);
  gulp.watch('./app/*.html', browserSync.reload);
  gulp.watch('./app/data/**/*.json', browserSync.reload);
  gulp.watch('./app/templates/**/*.html', ['templates']);
});

// clear
gulp.task('clean:dist', function () {
  return del.sync(['dist/**/*', '!dist/images/', '!dist/images/**/*'])
});

// build
gulp.task('build', function (cb) {
  runSequence('clean:dist',
    ['cssminifier', 'jsminifier', 'dataminifier', 'images', 'fonts'],
    'optimize:html',
    cb
  )
});

// deafult
gulp.task('default', function (cb) {
  runSequence(
    'sass',
    'jsconcat',
    ['browserSync', 'watch'],
    cb
  )
})
