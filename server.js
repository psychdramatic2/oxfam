var express = require('express');
var serveStatic = require('serve-static');
var	bodyParser = require('body-parser');
var app = express();

app.use(serveStatic(process.cwd()));
// app.use(serveStatic('/dist', {'index': ['index.html', 'index.htm']}))
app.use(express.static(__dirname + '/dist'));

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

app.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", server_port " + server_port )
});
